process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const webpush = require("web-push");
const express = require("express");
var cors = require("cors");
const bodyParser = require("body-parser");
const port = 3000;

// VAPID keys should be generated only once.
// let vapidKeys = webpush.generateVAPIDKeys();
let vapidKeys = {
  publicKey:
    "BBCcE3hX4YBHgZLyouMJYIFFSHOX-Drp0IKGMa7-59aZ0b5d9eFUz1w_V5B0z7gTwv7euVtKiXaBkk0cejuJ_nU",
  privateKey: "u1QbExkNMs0aHpbCanmZVdPpHkJBFQlCkF2FyouwpRY",
};
webpush.setVapidDetails(
  "mailto:yourmail@yourdomain.org",
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

const app = express();
const corsOpts = {
  origin: "*",

  methods: ["GET", "POST"],

  allowedHeaders: ["Content-Type"],
};
app.use(cors(corsOpts));
app.use(bodyParser.json());

const allSubscriptions = [];

/**
 * send generated vapid publickey
 */
app.route("/get-vapid-publickey").post((req, res) => {
  if (vapidKeys.publicKey) {
    res.status(200).json({ data: vapidKeys.publicKey });
  } else {
    res.status(400).json({ error: "public key not found on server" });
  }
});

app.route("/subscribe-user-notification").post((req, res) => {
  const sub = req.body;
  console.log("inside /subscribe-user-notification",sub);
  if (sub) {
    allSubscriptions.push(sub);
    res.status(200).json({ data: "success" });
  } else {
    res.status(400).json({ error: "got empty payload" });
  }
});

app.route("/send").post(sendNotification);

function sendNotification(req, res) {
  console.log("Total subscriptions", allSubscriptions.length);

  const notificationPayload = {
    notification: {
      title: "Angular News",
      body: "Newsletter Available!",
      icon: "assets/main-page-logo-small-hat.png",
      vibrate: [100, 50, 100],
      data: {
        dateOfArrival: Date.now(),
        primaryKey: 1,
      },
      actions: [
        {
          action: "explore",
          title: "Go to the site",
        },
      ],
    },
  };

  Promise.all(
    allSubscriptions.map((sub) => {
      console.log(sub);
      return webpush.sendNotification(sub, JSON.stringify(notificationPayload));
    })
  )
    .then(() =>
      res.status(200).json({ message: "notification sent successfully." })
    )
    .catch((err) => {
      console.error("Error sending notification, reason: ", err);
      res.status(500).send({
        message: err,
      });
    });
}

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
