import { NgModule } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [MatGridListModule, MatChipsModule, MatCardModule, MatButtonModule],
  exports: [MatGridListModule, MatChipsModule, MatCardModule, MatButtonModule],
})
export class MaterialSharedModule {}
