import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MaterialSharedModule } from './material-shared-module';

@NgModule({
  imports: [HttpClientModule,MaterialSharedModule],
  exports: [HttpClientModule,MaterialSharedModule]
})
export class SharedModule {}
