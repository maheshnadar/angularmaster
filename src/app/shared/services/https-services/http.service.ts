import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

// declare var $: any;

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  apiUrl = 'http://localhost:3000';
  self = this;
  constructor(private http: HttpClient) {}

  readCookie(name: any) {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  }

  get(path: string, parameters?: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({}),
    };
    const pathWithParameter = parameters
      ? path + '?' + this.transformRequest(parameters)
      : path;

    return this.http
      .get(this.apiUrl + pathWithParameter, httpOptions)
      .pipe(
        map(this.mapper.bind(this)),
        catchError(this.errorHandler.bind(this))
      );
  }

  downloadWithPostMethod(url: any, parameters: any) {
    const headers = {
      'Content-Type': 'application/json',
    };

    const httpOptions = {
      headers: new HttpHeaders(headers),
    };
    return this.http
      .post(this.apiUrl + url, parameters, {
        observe: 'response',
        responseType: 'blob' as 'json',
        headers: new HttpHeaders(headers),
      })
      .subscribe({
        next: (res: any) => {
          console.log(res.headers);
          console.log(res.headers.get('content-disposition')); // need to allow from backend to access content-disposition
          const contentDisposition = res.headers.get('content-disposition');
          const filename = contentDisposition
            .split(';')[1]
            .split('filename')[1]
            .split('=')[1]
            .trim()
            .split('"')[1];

          const downloadURL = window.URL.createObjectURL(res.body);
          const link = document.createElement('a');
          link.href = downloadURL;
          link.download = decodeURIComponent(filename);
          link.click();
        },
        error: (err) => {
          const self = this;
          const blob = window.URL.createObjectURL(err.error);
          console.log(err, blob);
        },
      });
  }

  downloadWithGetMethod(url: any) {
    const headers = {
      'Content-Type': 'application/json',
    };

    const httpOptions = {
      headers: new HttpHeaders(headers),
    };

    return this.http.get(this.apiUrl + url, {
      observe: 'response',
      responseType: 'blob',
      headers: new HttpHeaders(headers),
    });
  }

  delete(path: any, data: any): Observable<any> {
    const headers = {
      'Content-Type': 'application/json',
    };
    const httpOptions = {
      headers: new HttpHeaders(headers),
      body: data,
    };
    console.log('Data sent t0 delete' + JSON.stringify(httpOptions.body));
    return this.http
      .delete(this.apiUrl + path, httpOptions)
      .pipe(
        map(this.mapper.bind(this)),
        catchError(this.errorHandler.bind(this))
      );
  }

  transformRequest(obj: any) {
    const str = [];
    for (const p of Object.keys(obj)) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
    return str.join('&');
  }

  private mapper(response: any) {
    // this._loaderService.setLoaderState(false);
    return response;
  }

  post(path: any, parameters?: any): Observable<any> {
    const headers = {
      'Content-Type': 'application/json',
    };

    const httpOptions = {
      headers: new HttpHeaders(headers),
    };

    const finalParameters = parameters ? parameters : {};

    return this.http
      .post(this.apiUrl + path, finalParameters, httpOptions)
      .pipe(
        map(this.mapper.bind(this)),
        catchError(this.errorHandler.bind(this))
      );
  }

  put(path: any, parameters: any): Observable<any> {
    const headers = {
      'Content-Type': 'application/json',
    };
    const httpOptions = {
      headers: new HttpHeaders(headers),
    };
    return this.http
      .put(this.apiUrl + path, parameters, httpOptions)
      .pipe(
        map(this.mapper.bind(this)),
        catchError(this.errorHandler.bind(this))
      );
  }

  errorHandler(error: any) {
    return throwError(()=>error);

    const self = this;
    if (error['status'] === 400) {
      return throwError({
        code: 400,
        data: error.error,
      });
    } else if (error['status'] === 500) {
      return throwError({
        code: 500,
        data: error.error,
        message: 'Server error',
      });
    } else if (error['status'] === 401) {
      return throwError({
        code: 401,
        data: error.error,
        message: 'Unauthorized error',
      });
    } else {
      return throwError({
        code: 0,
        data: error.error,
        message: 'Something went wrong',
      });
    }
  }
}
