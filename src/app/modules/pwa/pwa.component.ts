import { Component, OnInit } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { HttpService } from 'src/app/shared/services/https-services/http.service';

@Component({
  selector: 'app-pwa',
  templateUrl: './pwa.component.html',
  styleUrls: ['./pwa.component.scss'],
})
export class PwaComponent implements OnInit {
  VAPID_PUBLIC_KEY :any= null

  constructor(private swPush: SwPush, private httpService: HttpService) {}

  ngOnInit(): void {
    this.swPush.notificationClicks.subscribe(({ action, notification }) => {
      // TODO: Do something in response to notification click.
      console.log(action, notification);
    });
  }


  getPublicKeyFromServer() {
    this.httpService.post('/get-vapid-publickey', {}).subscribe({
      next: (res) => {
        console.log(res);
        this.VAPID_PUBLIC_KEY=res.data
      },
      error: (err) => {
        console.log(err);
      },
    });
  }


  subscribeForNotification() {
    console.log("inside subscribeForNotification",this.VAPID_PUBLIC_KEY);
    this.swPush
      .requestSubscription({
        serverPublicKey: this.VAPID_PUBLIC_KEY,
      })
      .then((sub) => {
        console.log(sub);
        this.httpService.post('/subscribe-user-notification', sub).subscribe({
          next: (res) => {
            console.log(res);
          },
          error: (err) => {
            console.log(err);
          },
        });
      });
  }


  send(){
    this.httpService.post('/send', {}).subscribe({
      next: (res) => {
        console.log(res);
        this.VAPID_PUBLIC_KEY=res.data
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}
