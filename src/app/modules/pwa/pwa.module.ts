import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PwaRoutingModule } from './pwa-routing.module';
import { PwaComponent } from './pwa.component';
import { SharedModule } from 'src/app/shared/modules/shared-module';


@NgModule({
  declarations: [
    PwaComponent
  ],
  imports: [
    CommonModule,
    PwaRoutingModule,
    SharedModule
  ]
})
export class PwaModule { }
