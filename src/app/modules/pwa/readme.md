###### PWA ###############
- used for caching the resource (thus server wont be requesting the resource again on reload/revisit)
- uses service-worker to cache the resource
- service-worker wont work on http (required https to test the working or use localhost)
- service-worker act as a proxy to all http/https request
- every HTTP/HTTPS request you make in your PWA passes through the service-worker (SW)
- if the requested resource is on the SW cache, then it returns the content of the cache
- if the requested resource is not on the SW cache, then the SW performs the request to the server
- can configure push notification also
- gives the functionality to install the website as native app in handset (can configure icon also)


# install in angular
* ng add @angular/pwa
 - adds @angular/service-worker package to your project.
 - Updates the index.html  (Includes a link to add the manifest.webmanifest file,Adds a meta tag for theme-color)
 - creates manifest.webmanifest ( contains icon configuration and other setting)
 - creates file ngsw-config.json ( service worker configuration, which specifies the caching behaviors and other settings.)
 - imports ServiceWorkerModule in app.module
 - updates angular.json (includes files created above in angular project)

# run and test angular pwa app 
* ng build    (building project as "ng serve" wont have pwa)
* http-server -p 8080 -c-1 dist/<project-name>  ( using http-server to start the server)
* open url http://localhost:8080   (dont open with your PC IP , as service-worker wont work on http)
* turn off the internet and refresh the page ( you can see that all the resource are fetched from the service-worker)
* after few sec browser calls http://localhost:8080/ngsw.json to check for new verion
* if you deployed version 2 of the website.browser will load the old version first and check for new version in background and on the next visit will load the new version  
* reff -:
  - https://angular.io/guide/service-worker-getting-started
  - https://morioh.com/p/680222dda624

# Further improvements
 - make project responsive
 - inform your users if they are offline ,if you are working with some real time data
 - show loading indicator when ever necessary
 - Give a fixed height for image containers and show skeleton screens where applicable
 - If a request failed, show a retry component or show error message



##### Push notification ########
* Push API  (server sends notification message to client/browser,even when the site isn't focused or the browser is closed)
* Notifications API ( client/browser displays the notifications to the user)
* note -> 
  - we dont directly send notification message from server to browser,
  - instead we send request to google/mozilla server( known as a Browser Push Service) and they send notification message to browser

# Browser Push Service Providers
* eg- Firebase Cloud Messaging service  (manages all push message coming from Chrome browser)
* browser providers can block the notitication according to their policy

# VAPID 
* is a key pair of unique public and private key
* the public key is used as a unique server identifier for subscribing the user to notifications sent by that server
* the private key is used by the application server to sign messages, before sending them to the Push Service for delivery
* Generating a VAPID key pair
 - npm install web-push -g
 - web-push generate-vapid-keys --json    ( will give you private and public key,which can be used as vapid)

# In Angular (client/browser)
* SwPush(service-worker-push module is used to handle push notification)

# In Node (server)
* web-push package is used for handling push notification

# Working
* 1) in backend,generate vapid(public and private keypair) in backend and share the public key with frontend using api
 - code : 
	let vapidKeys = webpush.generateVAPIDKeys();    (generate only one time)  
	webpush.setVapidDetails(			(register the vapid)
	  "mailto:yourmail@yourdomain.org",
  	  vapidKeys.publicKey,
	  vapidKeys.privateKey
	);
* 2) in frontend,using the public key request Subscription in browser(ask notification permission) 
* 3) in frontend,if user allows permission then we will receive "PushSubscription Object"(contains info like endpoint,url,expiretime,etc).send that pushsubscription object to backend using api
 - code :
	this.swPush.requestSubscription({serverPublicKey: this.VAPID_PUBLIC_KEY}).then( //send the "PushSubscription Object" to server)
* 4) in backend,store that pushsubscrition object data in db/redis/globalvariable
* 5) in backend,when we want to send notification.use that stored "PushSubscription Object data" along with details(like title,icon,message,etc) to send notification to that browser.
 - code :
	webpush.sendNotification(PushSubscriptionObject, JSON.stringify({ notification : {title: "Angular News",body: "Newsletter Available!",..} }));
* 6) in frontend,notifcation will be shown in client system with details(like title,icon,message,etc) provided by backend
* 7) in frontend,register notificationClicks event which will be called when user clicks on the notification
 - code :
	 this.swPush.notificationClicks.subscribe(({ action, notification }) => {
     	 // TODO: Do something in response to notification click.
      	console.log(action, notification);
   	 });

# note 
- for some reason it is not working in chrome,works fine in firefox
- always check in browser with http://localhost ( dont check with local ip)(because notification works for localhost,if you use ip then you need ssl connection(https))
- angular need to be build with --prod to make service worker work in browser. and run the frontend in http-server
- in node use process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; to bypass certificate signing problem (wont cause problem when we work with proper ssl)



###### Module short description #######
- in this module pwa(progressive web app) is incorporated using (ng add @angular/pwa) 
- in a pwa app,app can work offline using the service worker.service worker caches the assets and make it available offline
- push notification also incorporated in this application,angular uses service worker to show push notificaion
- for push notification, node server is also used along with frontend (check above working section)
- run the node server using (npm run run-notification-server)
- in ui component,there is a three button. 
    First one to get public key from backend.
    Second is to subscribe to push notificaiton using that public key and send the meta data to server.
    Third button is to ask server to send notification
- node server is needed for sending the notificaion across browser