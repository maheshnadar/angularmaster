###### NGRX ###################
- ng add @ngrx/store@latest
- NGRX is a package used for state management
- it creates a global vaiable (aka state) which can be accessed throughout the component
- can share data with multiple component
- it solves the problem of synchronizing the data throughout the component 
(ie it data/state changes in one component,changes sholud reflect in another component in realtime)
- it uses observable 


## Process goes likes this ## 
- you will define a action(eg- increment)
- you will define a reducer which captures the action and and changes the state ( createReducer(initialState,on(increment, (state) => state + 1));)
- you will define the StoreModule along with Reducer in module (StoreModule.forRoot({counts: counterReducer}, {})
- you will invoke the Store in component which can be used to select state and dispatch action (  constructor(private store: Store<{ counts: number }>) { }
- you will dispatch the action where ever the action need to be performed ( this.store.dispatch(increment()); )  
- you will select the state from the store ( this.count$ = store.select('counts');)
## END - Process goes likes this ## 

# State 
- state is a global variable which will be shared accros the component through ngrx
- State is accessed with the Store, an observable of state and an observer of actions.

# Actions 
- it is like defining a event which will be used to emit data from component
- Actions describe unique events that are dispatched from components and services.
-> export const increment = createAction('[Counter Component] Increment',props<{}>());
-> this.store.dispatch(increment());   -> dispating a event

# Reducers 
- used to capture the emitted event and return the new state
- State changes are handled by pure functions called reducers that take the current state and the latest action to compute a new state.
-> export const counterReducer = createReducer(initialState,on(increment, (state) => state + 1));

# StoreModule
- import storemodule in app.module with above reducer
-> StoreModule.forRoot({counts: counterReducer}, {})   -> 'counts' key label will be used in selector


# Selectors 
- to select state from store (its more like a selecting data from database )
- Selectors are pure functions used to select, derive and compose pieces of state.
-> this.count$ = store.select('counts');    
-> another syntax -> this.count$ = store.select((select)=>select.count);
-> this.count$.subscribe(x=>{console.log(x);})     -> changed value will be called here

# createSelector
- if you have two array stored in state and if you want to add both array then you can create a seperate selector to implement the logic
- eg - > 
export const selectBooks = createFeatureSelector<Array<Book>>('books');     --> same as store.select('books');
export const selectCollectionState = createFeatureSelector<Array<string>>('collection');  --> selector for collection
export const selectBookCollection = createSelector(selectBooks,selectCollectionState,(books, collection) => { <login> }); --> this selector will combine books with collectionId and will give the result
bookCollection$ = this.store.select(selectBookCollection);   --> to select the selector

# Store
- NgRx Store is mainly for managing global state across an entire application
- you will select and subscribe to the state to receive changes(this.count$ = store.select('counts'); this.count$.subscribe(x=>{console.log(x);}))


## ----- EFFECTS ----- ##
- need to install seperatly ng add @ngrx/effects@latest
- Used to ask for data from api,component,etc or to perform asynchronous operation
- eg - in rta project you can use "effect" to get data from inward,viewsupport when clicked on save
- Effects are where you handle tasks such as fetching data, long-running tasks that produce multiple events,
- Effects are long-running services that listen to an observable of every action dispatched from the Store.
- Effects perform tasks, which are synchronous or asynchronous and return a new action
- Automatically called when type(ie '[Movies Page] Load Movies') is dispatched
- need to include in NgModule and import as EffectsModule.forRoot([MovieEffects])
--> 
@Injectable()
export class MovieEffects {
  loadMovies$ = createEffect(() => this.actions$.pipe(
    ofType('[Movies Page] Load Movies'),
    mergeMap(() => this.moviesService.getAll()
      .pipe(
        map(movies => ({ type: '[Movies API] Movies Loaded Success', payload: movies })),
        catchError(() => EMPTY)
      ))
    )
}
- steps goes like this
-->createservice for api,
-->create effect and define the type(reff ofType - eg '[Movies Page] Load Movies') and call the api and dispatch the successAction with data
--> include the effect in module
-->in component dispatch the type (reff ofType - eg '[Movies Page] Load Movies')

- process goes like this
--> compoent dispatch the type (asking for data)
--> effect captures the type dispatched and calls the serviceapi  (loads the data)
--> after getting the data/state serviceapi it dispatchs the data/state       ( changes the state in store)
--> we get data/state from store using select


## END ----- EFFECTS ----- ##


###### Module short description #######
- StoreModule,EffectsModule imported in app.module.ts
- ngrx-home componenet is the main component which is loaded and shows the working of ngrx
- simple example of counter increment from one component and reflecting in another component is incorporated using ngrx action,reducer,selector
- inside advance-ngrx folder , more advance use of ngrx is incorporated like effect,api call,createselector,etc