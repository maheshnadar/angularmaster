import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { GoogleBooksService } from '../advance-ngrx/book-list/books.service';
import { addBook, removeBook, retrievedBookList } from '../advance-ngrx/state/books.actions';
import { selectBookCollection, selectBooks } from '../advance-ngrx/state/books.selectors';

@Component({
  selector: 'app-ngrx-home',
  templateUrl: './ngrx-home.component.html',
  styleUrls: ['./ngrx-home.component.scss']
})
export class NgrxHomeComponent implements OnInit {

  books$ = this.store.select(selectBooks);
  bookCollection$ = this.store.select(selectBookCollection);
 
  onAdd(bookId: any) {
    console.log(bookId as string);
    this.store.dispatch(addBook({ bookId }));
  }
 
  onRemove(bookId: string) {
    this.store.dispatch(removeBook({ bookId }));
  }
 
  constructor(
    private booksService: GoogleBooksService,
    private store: Store
  ) {}
 
  ngOnInit() {
    // --> Using service Directly 
    // this.booksService
    // .getBooks()
    // .subscribe((books) => this.store.dispatch(retrievedBookList({ books })));
    //--> Using Effects
    this.store.dispatch({ type: '[Movies Page] Load Movies' });
  }
}
