import { Injectable } from '@angular/core';
import { Actions,createEffect,ofType } from '@ngrx/effects';
import { GoogleBooksService } from './book-list/books.service';
import { retrievedBookList, retrievedBookListFailed } from './state/books.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import { EMPTY } from 'rxjs';


@Injectable()
export class BookEffectService {
    loadBooks$ = createEffect(() =>
    this.actions$.pipe(
      ofType('[Movies Page] Load Movies'),
      switchMap(() => this.bookService.getBooks().pipe(
        map(books => retrievedBookList({books}) ),
        catchError(() => EMPTY
        )
      ))
    )
  );
  constructor(private actions$: Actions,private bookService:GoogleBooksService) {
      
  }
}
