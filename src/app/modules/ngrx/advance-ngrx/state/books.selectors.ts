import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Book } from '../model/books.model';
 
export const selectBooks = createFeatureSelector<Array<Book>>('books');
 
export const selectCollectionState = createFeatureSelector<
  Array<string>
>('collection');
 
export const selectBookCollection = createSelector(
  selectBooks,
  selectCollectionState,
  (books, collection) => {
    console.log("selectBookCollection",books,collection)
    return collection.map((id) => books.find((book) => book.id === id));
  }
);