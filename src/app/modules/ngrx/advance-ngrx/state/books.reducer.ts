import { createReducer, on } from '@ngrx/store';
import { Book } from '../model/books.model';

import { retrievedBookList, retrievedBookListFailed } from './books.actions';

export const initialState: Array<Book> = [];

export const booksReducer = createReducer(
  initialState,
  on(retrievedBookList, (state, { books }) => books),
  on(retrievedBookListFailed, (state,) => state)
);