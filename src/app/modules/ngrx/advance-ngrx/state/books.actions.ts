import { createAction, props } from '@ngrx/store';
import { Book } from '../model/books.model';

export const addBook = createAction(
  '[Book List] Add Book',
  props<{ bookId: string }>()
);

export const removeBook = createAction(
  '[Book Collection] Remove Book',
  props<{ bookId: string }>()
);


export const retrievedBookList = createAction(
  '[Book List/API] Retrieve Books Success',
  props<{ books: Array<Book> }>()
);

export const retrievedBookListFailed = createAction(
  '[Book Collection] retrievedBookListFailed'
);

export const retrievedBookList1 = createAction(
  '[Book List] retrive books data',
  props<{ books: Array<Book> }>()
);
