import { Book } from "../model/books.model";

export interface AppState {
  books: Array<Book>;
  collection: Array<string>;
}