import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-my-counter-display',
  templateUrl: './my-counter-display.component.html',
  styleUrls: ['./my-counter-display.component.scss'],
})
export class MyCounterDisplayComponent implements OnInit {
  count$:Observable<number>;
  constructor(private store: Store<{ counts: number }>) {
    this.count$ = store.select('counts');
  }

  ngOnInit(): void {}
}
