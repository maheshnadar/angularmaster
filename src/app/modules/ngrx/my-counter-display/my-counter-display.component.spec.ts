import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCounterDisplayComponent } from './my-counter-display.component';

describe('MyCounterDisplayComponent', () => {
  let component: MyCounterDisplayComponent;
  let fixture: ComponentFixture<MyCounterDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyCounterDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCounterDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
