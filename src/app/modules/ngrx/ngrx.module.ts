import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgrxRoutingModule } from './ngrx-routing.module';
import { NgrxHomeComponent } from './ngrx-home/ngrx-home.component';
import { MyCounterComponent } from './my-counter/my-counter.component';
import { MyCounterDisplayComponent } from './my-counter-display/my-counter-display.component';
import { MaterialSharedModule } from 'src/app/shared/modules/material-shared-module';
import { BookCollectionComponent } from './advance-ngrx/book-collection/book-collection.component';
import { BookListComponent } from './advance-ngrx/book-list/book-list.component';
import { SharedModule } from 'src/app/shared/modules/shared-module';


@NgModule({
  declarations: [NgrxHomeComponent, MyCounterComponent, MyCounterDisplayComponent,BookCollectionComponent,BookListComponent],
  imports: [
    CommonModule,
    NgrxRoutingModule,SharedModule
  ]
})
export class NgrxModule { }
