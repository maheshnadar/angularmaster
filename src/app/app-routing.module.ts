import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'ngrx',
    loadChildren: () =>
      import('./modules/ngrx/ngrx.module').then((m) => m.NgrxModule),
  },
  {
    path: 'pwa',
    loadChildren: () =>
      import('./modules/pwa/pwa.module').then((m) => m.PwaModule),
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    // preloadingStrategy: PreloadAllModules,
    useHash: true
  })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
