import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialSharedModule } from './shared/modules/material-shared-module';
import { MenuComponent } from './shared-component/menu/menu.component';
import { FooterComponent } from './shared-component/footer/footer.component';
import { StoreModule } from '@ngrx/store';
import { counterReducer } from './modules/ngrx/counter.reducer';
import { booksReducer } from './modules/ngrx/advance-ngrx/state/books.reducer';
import { collectionReducer } from './modules/ngrx/advance-ngrx/state/collection.reducer';
import { SharedModule } from './shared/modules/shared-module';
import { EffectsModule } from '@ngrx/effects';
import { BookEffectService } from './modules/ngrx/advance-ngrx/bookEffectService';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialSharedModule,
    SharedModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({counts: counterReducer,books: booksReducer, collection: collectionReducer}, {}),
    EffectsModule.forRoot([BookEffectService]),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
